# Blueprism Demo Package

Sample VBOs to demonstrate Blue Prism integration with CyberArk CCP.

## Environment 

This integration has been tested with: 
- **Blue Prism** - Robotic Process Automation Software - v6.10.3
- **CyberArk** - Privileged Access Security - v12.2.0 

## Prerequisites

- [CyberArk Connector](https://digitalexchange.blueprism.com/dx/entry/10326/solution/blue-prism-cyberark-integration), also available on [CyberArk Marketplace](https://cyberark-customers.force.com/mplace/s/#a3550000000EiAKAA0-a3950000000jjSXAAY)!
- [Blue Prism VBO wrapper for SSH.NET library](https://digitalexchange.blueprism.com/dx/entry/9648/solution/sshnet-2)
- [Blue Prism VBO for manipulating XML](https://digitalexchange.blueprism.com/dx/entry/3439/solution/utility---xml)

![prerequisites.png](./screenshots/prerequisites.png)

Follow User Guides, in particular:

1. Page 10-15, section *REGISTER THE CYBERARK WEB SERVICE IN BLUE PRISM* from [CyberArk integration guide](https://cyberark-customers.force.com/mplace/s/#a3550000000EiAKAA0-a3950000000jjSXAAY)
2. Page 5 from [SSH.NET User Guide](https://digitalexchange.blueprism.com/dx/entry/9648/solution/sshnet-2)


## Usage

The Blue Prism demo package contains 2 BP objects and 1 BP process:
```
- BPA Object - List_Files_On_Remote_SSH_Server.bpobject
- BPA Object - List_Files_One_Remote_SSH_Server_CYBR.bpobject
- BPA Process - CYBR_Get_Password.bpprocess
```

### Import files
Import the above files in Blue Prism.

![project_files.png](./screenshots/project_files.png)

### List_Files_On_Remote_SSH_Server
Then in Blue Prism studio, open the List_Files_On_Remote_SSH_Server Object and: 

1. Go to the `List_Files_On_Remote_SSH_Server` page
2. Edit the Blue Prism items accordingly
3. Right click on `Start` and then on `Set Next Stage`
4. Click on the green arrow 
5. Check results

![list_files_on_remote_ssh_server.png](./screenshots/list_files_on_remote_ssh_server.png)

You should be able to list files from a remote server directory.

### List_Files_On_Remote_SSH_Server_CYBR
Then in Blue Prism studio, open the List_Files_On_Remote_SSH_Server_CYBR Object and: 

1. Go to the `List_Files_On_Remote_SSH_Server` page
2. Edit the Blue Prism items accordingly

![cyberark_input_parameters.png](./screenshots/cyberark_input_parameters.png)

3. Right click on `Start` and then on `Set Next Stage`
4. Click on the green arrow 
5. Check results

![list_files_on_remote_ssh_server_cybr.png](./screenshots/list_files_on_remote_ssh_server_cybr.png)

You should be able to list files from a remote server directory as well, but this time the password has been securely retrieved from CyberArk! 

## Licensing

Copyright © 2021 CyberArk Software Ltd

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
